// ==UserScript==
// @name rsb-userscript
// @description Выставляет логин и фокус в правильное поле для ИБ РСБ.
// @author Vladimir N. Indik
// @license GPLv3
// @version 1.1
// @icon https://online.rsb.ru/favicon.ico
// @include https://online.rsb.ru/hb/faces/system/login/rslogin.jsp*
// @include https://online.rsb.ru/hb/faces/rs/remittances/RSRemittance_internal.jspx
// @include https://online.rsb.ru/hb/faces/rs/remittances/RSRemittance_internal_confirm.jspx
// @include https://online.rsb.ru/hb/faces/rs/payments/RSPayments.jspx
// @include https://online.rsb.ru/hb/faces/rs/payments/RSPaymentRequest.jspx
// @include https://online.rsb.ru/hb/faces/rs/RSIndex.jspx
// @include https://online.rsb.ru/hb/faces/rs/remittances/RSBankClientPmt.jspx
// @include https://online.rsb.ru/hb/faces/rs/remittances/RSRemittance.jspx
// ==/UserScript==
// [1] Оборачиваем скрипт в замыкание, для кроссбраузерности (opera, ie) (см. http://habrahabr.ru/post/129343/)
(function (window, undefined) {  // [2] нормализуем window
    var w;
    if (typeof unsafeWindow != undefined) {
        w = unsafeWindow
    } else {
        w = window;
    }

    // [3] не запускаем скрипт во фреймах
    // без этого условия скрипт будет запускаться несколько раз на странице с фреймами
    if (w.self != w.top) {
        return;
    }
    // [4] дополнительная проверка наряду с @include
    if (/https:\/\/online.rsb.ru\/hb\/faces\/system\/login\/rslogin.jsp/.test(w.location.href)) {
        var $ = w.jQuery;
        var $login = $('#user_login_internet'),
            $password = $('#user_password_internet');
        var $loginLabel = $login.parent().children('label'),
            $passwordLabel = $password.parent().children('label');
        var $user = 'indik'; //Сюда вписываем свой логин

        //Скрываем заголовок "Логин"
        $loginLabel.hide();
        //Выставляем правильный логин
        $login.val($user);
        //Костылик для хрома
        if(!$('#popup_restore').is(':visible')) {
            //Скрываем заголовок "Пароль"
            $passwordLabel.hide();
            //Выставляем правильный фокус. Лёха, привет!
            $password.focus();
        }

        //Выставляем правильный фокус при закрытии окна о неверном пароле
        $('#close_popup_txt').click(function(){
            $passwordLabel.hide();
            $password.focus();
        });

        //Нажимаем кнопку "Войти" при нажатии Enter в поле ввода
        $('#smsinp').keypress(function(e) {
            //Прилетело нажатие Enter
            if (e.which == 13) {
                $('#L2').click();
            }
        });
    }

    //Подтверждение платежей, переводов и т.д.
    if (/https:\/\/online.rsb.ru\/hb\/faces\/rs/.test(w.location.href)) {
        if(unsafeWindow.$('[id="mainform:_id78:0:number1"]').length>0) {
            //Выставляем фокус в первое поле ввода
            unsafeWindow.$('[id="mainform:_id78:0:number1"]').focus();
        } else if(unsafeWindow.$('[id="searchfield"]').length>0) {
            //Выставляем фокус в поле для поиска
            unsafeWindow.$('[id="searchfield"]').focus();
        } else if(unsafeWindow.$('[id="mainform:phoneNumber1"]').length>0) {
           //Выставляем фокус в поле с номером поиска
           unsafeWindow.$('[id="mainform:phoneNumber1"]').focus();
        }

        //Нажимаем кнопку "Подтвердить" при нажатии Enter в поле ввода
        unsafeWindow.$('[id="mainform:pincode1"]').live('keypress', function(e) {
            //Прилетело нажатие Enter
            if (e.which == 13) {
                unsafeWindow.$('[id="mainform:transfer"]').click();
            }
        });

        //Нажимаем кнопку "Перевести" при нажатии Enter в поле ввода
        unsafeWindow.$('[id="mainform:remittance_sum"]').live('keypress', function(e) {
            //Прилетело нажатие Enter
            if (arguments[0].which == 13) {
                unsafeWindow.$('[id="mainform:further"]').click();
            }
        });
    }

})(window);
